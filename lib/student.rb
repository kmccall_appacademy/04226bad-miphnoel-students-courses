class Student
  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(new_course)
    return nil if @courses.include?(new_course)
    raise "Course schedule conflict" if has_conflict?(new_course)

    @courses << new_course
    new_course.students << self
  end

  def course_load
    credits_per_department
  end

  private

  def has_conflict?(new_course)
    @courses.any? { |course| course.conflicts_with?(new_course) }
  end

  def credits_per_department
    credit_counts = Hash.new(0)

    @courses.each do |course|
      credit_counts[course.department] += course.credits
    end

    credit_counts
  end
end
